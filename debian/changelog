emperor (1.0.4+ds-1) unstable; urgency=medium

  * Team Upload
  * Ack NMU
  * New upstream version 1.0.4+ds
  * Add an explicit runtime dependency on python3-pkg-resources
  * Refresh patches

 -- Alexandre Detiste <tchet@debian.org>  Fri, 21 Feb 2025 21:54:15 +0100

emperor (1.0.3+ds-9.1) unstable; urgency=medium

  * Non-maintainer upload
  * Move from jsdoc-toolkit to node-jsdoc2 (Closes: #1074593)

 -- Bastian Germann <bage@debian.org>  Mon, 01 Jul 2024 18:07:00 +0000

emperor (1.0.3+ds-9) unstable; urgency=medium

  * Team Upload.
  * Add patch to fix FTBFS with pandas 2.0 (Closes: #1050144)

 -- Nilesh Patra <nilesh@debian.org>  Sun, 04 Feb 2024 16:18:31 +0530

emperor (1.0.3+ds-8) unstable; urgency=medium

  * Team upload.

  [ Alexandre Detiste ]
  * Remove extraneous dependency on python3-future
    Closes: #1058565

  [ Andreas Tille ]
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Fix test

 -- Andreas Tille <tille@debian.org>  Fri, 15 Dec 2023 22:10:49 +0100

emperor (1.0.3+ds-7exp1) experimental; urgency=medium

  * Team upload
  * Build against scipy 1.10 and skbio in experimental
    Closes: #1029689
  * Fix typo in d/control: Section
  * Respect nocheck in DEB_BUILD_OPTIONS
  * Silence lintian about fonts

 -- Andreas Tille <tille@debian.org>  Thu, 26 Jan 2023 13:57:41 +0100

emperor (1.0.3+ds-7) unstable; urgency=medium

  * Team upload.
  * Section: python
  * DEP3
  * Add upstream patch fpr pandas 1.5 compatibility
    Closes: #1024820
  * Drop unused lintian-overrides
  * Fix description

 -- Andreas Tille <tille@debian.org>  Thu, 26 Jan 2023 07:54:27 +0100

emperor (1.0.3+ds-6) unstable; urgency=medium

  * Team upload.
  * Packaging update
  * Standards-Version: 4.6.2 (routine-update)
  * Ignoring the test is no solution to become buildable with pandas 1.5

 -- Andreas Tille <tille@debian.org>  Fri, 13 Jan 2023 07:33:05 +0100

emperor (1.0.3+ds-5) UNRELEASED; urgency=medium

  * Team upload.

 -- Andreas Tille <tille@debian.org>  Fri, 13 Jan 2023 07:03:30 +0100

emperor (1.0.3+ds-4) unstable; urgency=medium

  * Team Upload.
  * Add skip-not-installable restriction
  * Move skbio to depends

 -- Nilesh Patra <nilesh@debian.org>  Mon, 12 Sep 2022 22:45:56 +0530

emperor (1.0.3+ds-3) unstable; urgency=medium

  [ Mohammed Bilal ]
  * Team upload.
  * Add autopkgtests
  * add lintian override
  * routine-update: Standards-Version: 4.6.1
  * routine-update: Remove trailing whitespace in debian/changelog
  * Update renamed lintian tag names in lintian overrides.

  [ Nilesh Patra ]
  * d/salsa-ci.yml: Disable i386 build due to skbio being uninstallable

 -- Mohammed Bilal <mdbilal@disroot.org>  Fri, 26 Aug 2022 22:02:44 +0530

emperor (1.0.3+ds-2) unstable; urgency=medium

  * Team upload.
  * Source-only upload
  * Remove inactive uploaders

 -- Andreas Tille <tille@debian.org>  Wed, 17 Nov 2021 07:39:43 +0100

emperor (1.0.3+ds-1) unstable; urgency=medium

  [ Liubov Chuprikova ]
  * Initial release (Closes: #843022)

  [ Steffen Moeller ]
  * Bumped to latest upstream version
  * Standards-Version: 4.6.0
  * Tests are fine with PYTHONPATH set.
  * Changed repacksuffix from +dfsg to +ds
  * depend on libjs-qunit

  [ Routine-Update ]
  * debhelper-compat 13
  * Secure URI in copyright format
  * Add salsa-ci file
  * Rules-Requires-Root: no
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Steffen Moeller <moeller@debian.org>  Sun, 26 Sep 2021 21:14:28 +0200
